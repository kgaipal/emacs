(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(Buffer-menu-name-width 35)
 '(auto-save-default t)
 '(custom-enabled-themes '(yet-another-dark))
 '(custom-safe-themes
   '("d97eac2de6a097b112c277575917169bf93e428dcf25846621512d7bffd059f8" "15290cc4f813a717dfe953657368d6ba55226e9b98f10cee06b5fcd582b8340e" default))
 '(dired-listing-switches "-pgGhB")
 '(dired-mode-hook '(toggle-truncate-lines turn-on-occur-x-mode))
 '(fill-column 90)
 '(fringe-mode '(1 . 1) nil (fringe))
 '(global-subword-mode t)
 '(grep-use-null-device nil)
 '(highlight-symbol-colors '("yellow"))
 '(ido-enable-flex-matching t)
 '(ido-mode 'buffer nil (ido))
 '(indent-tabs-mode nil)
 '(initial-buffer-choice nil)
 '(json-reformat:indent-width 2 t)
 '(kill-whole-line t)
 '(make-backup-files nil)
 '(mode-line-in-non-selected-windows t)
 '(package-selected-packages
   '(activities ess zpresent ada-ref-man ada-mode ace-window ack 0blayout magit auto-complete yaml-mode ws-butler web-mode typescript-mode ripgrep restart-emacs powershell omnisharp occur-x markdown-mode magit-todos magit-lfs json-mode indent-guide highlight-symbol fold-this fic-mode edit-at-point deadgrep counsel buffer-move anzu annotate-depth))
 '(read-file-name-completion-ignore-case t)
 '(recentf-mode 1)
 '(scroll-bar-mode nil)
 '(scroll-conservatively 100)
 '(sentence-end-double-space nil)
 '(show-paren-mode t)
 '(split-width-threshold 180)
 '(tab-width 8)
 '(tool-bar-mode nil)
 '(tramp-auto-save-directory "/tmp")
 '(warning-suppress-types '((emacs)))
 '(which-func-format '("{" which-func-current "}")))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(font-lock-constant-face ((t (:foreground "dim gray"))))
 '(font-lock-doc-face ((t (:inherit font-lock-string-face :foreground "dim gray")))))
